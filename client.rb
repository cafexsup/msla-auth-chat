gem 'dynamics_crm'
gem 'jwt'
gem 'openssl'
gem 'sinatra'

require "openssl"
require "jwt"
require "sinatra"
require "dynamics_crm"
require "Date"

@@rsa_private = OpenSSL::PKey::RSA.generate 2048
@@rsa_public = @@rsa_private.public_key


get "/authchat" do

	@org = "cafexdevsup"

	@client = DynamicsCRM::Client.new({organization_name: @org})
	@client.authenticate('cafexdevsup@cafexdevsup.onmicrosoft.com', 'Lionking10')

	# @users = @client.retrieve_multiple('contact', [["firstname", "Equal", "Alex"]])

	# @user = @users.entities.first.id.upcase

	@payload = { 
				  # :sub => @users.entities.first.id.upcase,
				  #:sub => "63A0E5B9-88DF-E311-B8E5-6C3BE5A8B200", #Alex
				  :sub => "C7A2E5B9-88DF-E311-B8E5-6C3BE5A8B200".upcase, #Jim
		  		  "preferred_username" => "Jeff",
				  "phone_number" => "",
				  "given_name" => "Jim",
				  "family_name" => "",
				  "email" => "jim@cafex.com",
				  iss: "cafex.com",
				  iat: DateTime.now.strftime('%Q').to_i,
	 			  exp: DateTime.now.strftime('%Q').to_i + 7200
	 	} 

	@rsa_private = @@rsa_private
	@rsa_public = @@rsa_public

	@token = JWT.encode @payload, @rsa_private, 'RS256'
	token = JWT.decode @token, @rsa_public, true, { :algorithm => 'RS256' }

	erb :authchat

end

